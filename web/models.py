# models.py


import datetime
from app import db


class Counter(db.Model):

    __tablename__ = 'counter'

    id = db.Column(db.Integer, primary_key=True)
    row_added = db.Column(db.DateTime, nullable=False)

    def __init__(self):
        self.row_added = datetime.datetime.now()
