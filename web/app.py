# app.py


from flask import Flask
from flask import request, render_template
from flask_sqlalchemy import SQLAlchemy
from config import BaseConfig


app = Flask(__name__)
app.config.from_object(BaseConfig)
db = SQLAlchemy(app)


from models import *


@app.route('/', methods=['GET', 'POST'])
def index():
    # if request.method == 'POST':
    counter = Counter()
    db.session.add(counter)
    db.session.commit()
    current_count = Counter.query.count()
    return render_template('index.html', count=current_count)


if __name__ == '__main__':
    app.run()
